﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Models;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository productRepository;
        public int PageSize = 3;
        /// <summary>
        /// Tworzy kontroler dla modelu produktu
        /// </summary>
        /// <param name="productRepositoryParam">Repozytroium produktów (IEnumerable)</param>
        public ProductController(IProductRepository productRepositoryParam)
        {
            this.productRepository = productRepositoryParam;
        }
        /// <summary>
        /// Metoda zwracająca widok kolekcji produktów wraz z stronicowaniem
        /// Przekazuje do widoku model widoku, zapewniajacy widokowi dostep do danych o stronicowaniu
        /// oraz o kolekcji produktów
        /// </summary>
        /// <returns></returns>
        public ViewResult List(string category, int page = 1)
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = productRepository.Products.
                    Where(p => p.Category == category || category == null).
                    OrderBy(p => p.ProductId).
                    Skip((page - 1) * PageSize).
                    Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ?
                    productRepository.Products.Count() :
                    productRepository.Products.Where(p => p.Category == category).Count()

                },
                CurrentCategory = category
            };
            return View(model);
        }
    }
}