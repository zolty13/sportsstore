﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain.Abstract;

namespace SportsStore.WebUI.Controllers
{
    public class NavigationController : Controller
    {
        private IProductRepository productRepository;

        public NavigationController(IProductRepository productRepositoryParam)
        {
            productRepository = productRepositoryParam;
        }

        // GET: Navigation
        public PartialViewResult Menu(string chosenCategory = null)
        {
            ViewBag.SelectedCategory = chosenCategory;
            IEnumerable<string> categories = productRepository.Products.
                Select(prod => prod.Category).
                Distinct().
                OrderBy(cat => cat);
            return PartialView(categories);
        }
    }
}