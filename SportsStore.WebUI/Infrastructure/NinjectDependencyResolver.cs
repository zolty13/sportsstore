﻿using Moq;
using Ninject;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain.Concrete;

namespace SportsStore.WebUI.Infrastructure
{
    /// <summary>
    /// Klasa rozwiazujaca zaleznosci Ninject
    /// </summary>
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        /// <summary>
        /// Tworzy obiekt klasy przechowujacy i tworzacy powiazania Ninject
        /// </summary>
        /// <param name="kernelParam">Jądro Ninject</param>
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        /// <summary>
        /// Metoda zwracajaca instancje rzadanego typu serwisu Ninject 
        /// </summary>
        /// <param name="serviceType">Rządany typ serwisu</param>
        /// <returns>Zwraca Instancje serwiu</returns>
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        /// <summary>
        /// Metoda zwracajaca kolekcje rzadanego typu serwisiu Ninject
        /// </summary>
        /// <param name="serviceType">Rzadany typ serwisu</param>
        /// <returns>Zwraca kolekcje serwisow</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            //tu umiesc powiazania
            this.kernel.Bind<IProductRepository>().To<EFProductRepository>();//ToConstant(mock.Object);
        }

    }
}