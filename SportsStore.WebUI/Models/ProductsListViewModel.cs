﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebUI.Models
{
    /// <summary>
    /// Klasa View Model opakowująca dane z kolekcji produktów, dane stronicowania
    /// oraz bieżącą kategorię
    /// </summary>
    public class ProductsListViewModel
    {
        /// <summary>
        /// Kolekcja produktów
        /// </summary>
        public IEnumerable<Product> Products { get; set; }
        /// <summary>
        /// Informacje o stronicowaniu
        /// </summary>
        public PagingInfo PagingInfo { get; set; }
        /// <summary>
        /// Bieżąca kategoria filtrowania
        /// </summary>
        public string CurrentCategory{ get; set; }
    }
}