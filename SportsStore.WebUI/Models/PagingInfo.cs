﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebUI.Models
{
    /// <summary>
    /// Informacje potrzbne przy stronicowaniu
    /// </summary>
    public class PagingInfo
    {
        /// <summary>
        /// Całkowita ilość przedmiotów 
        /// </summary>
        public int TotalItems { get; set; }
        /// <summary>
        /// Ilość przedmiotów na stronę
        /// </summary>
        public int ItemsPerPage { get; set; }
        /// <summary>
        /// Strona bieżąca
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Ilość stron
        /// </summary>
        public int TotalPages
        {
            get { return (int)Math.Ceiling((double)TotalItems / ItemsPerPage); }
        }
    }
}