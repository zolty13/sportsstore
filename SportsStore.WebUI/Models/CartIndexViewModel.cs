﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebUI.Models
{
    public class CartIndexViewModel
    {
        /// <summary>
        /// instacncja koszyka
        /// </summary>
        public Cart Cart { get; set; }
        /// <summary>
        /// Zwracany adres Url
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}