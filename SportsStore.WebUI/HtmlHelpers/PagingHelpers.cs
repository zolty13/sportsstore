﻿using SportsStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.HtmlHelpers
{
    /// <summary>
    /// Klasa statyczna z metodami rozszerzajacymi dla klasy HtmlHelper
    /// </summary>
    public static class PagingHelpers
    {
        /// <summary>
        /// Metoda rozszerzajaca klasy HtmlHelper
        /// </summary>
        /// <param name="htmlHelper">Klasa rozszerzana</param>
        /// <param name="paginInfo">Informacje dla stronicowania</param>
        /// <param name="pageUrl">Metoda zwracająca adres strony</param>
        /// <returns>Zwraca kod html potrzebny do stronicowania</returns>
        public static MvcHtmlString PageLinks(this HtmlHelper htmlHelper,
            PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for(int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tagbuilder = new TagBuilder("a");
                tagbuilder.MergeAttribute("href", pageUrl(i));
                tagbuilder.InnerHtml = i.ToString();
                if(i == pagingInfo.CurrentPage)
                {
                    tagbuilder.AddCssClass("selected");
                    tagbuilder.AddCssClass("btn-primary");
                }
                tagbuilder.AddCssClass("btn btn-default");
                result.Append(tagbuilder.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}