﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Domain.Entities;

namespace SportsStore.Domain.Abstract
{
    /// <summary>
    /// Interfejs z funckjonalnościa kolekcji encji Produkt 
    /// wzorzec repozytorium 
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// Kolekcja produktów 
        /// </summary>
        IEnumerable<Product> Products { get; }
    }
}
