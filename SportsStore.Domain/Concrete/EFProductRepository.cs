﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using System.Data.Entity;

namespace SportsStore.Domain.Concrete
{
    /// <summary>
    /// Wzorzec repozytorium - kolekcja instancji Produkt
    /// </summary>
    public class EFProductRepository : IProductRepository
    {
        private EFDbContext context = new EFDbContext();
        /// <summary>
        /// Kolekcja produktów 
        /// </summary>
        public IEnumerable<Product> Products
        {
            get { return context.Products; }
        }
    }
}
