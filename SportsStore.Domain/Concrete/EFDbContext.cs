﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Concrete
{
    /// <summary>
    /// Klasa będąca kontekstem do bazy danych
    /// </summary>
    public class EFDbContext : DbContext
    {
        /// <summary>
        /// Kolekcja produktów
        /// </summary>
        public DbSet<Product> Products { get; set; }
    }
}
