﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    /// <summary>
    /// Koszyk za zakupy, realizuje dodawanie produktów do koszyka w roznej ilosci, usuwanie produktu z koszyka 
    /// </summary>
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();
        /// <summary>
        /// Metoda która dodaje liczbe produktu productParam do koszyka w ilosci quantityParam
        /// tworzy nowa pozycje (element zamowienia) dla produktu w koszyku, jesli wczesniej nie dodano tego produktu do koszyka
        /// </summary>
        /// <param name="productParam">Produkt</param>
        /// <param name="quantityParam">ilość</param>
        public void AddItem(Product productParam, int quantityParam)
        {
            CartLine cartLine = lineCollection.
                Where(l => l.Product.ProductId == productParam.ProductId).
                FirstOrDefault();
            if (cartLine == null)
                lineCollection.Add(new CartLine
                {
                    Product = productParam,
                    Quantity = quantityParam
                });
            else
                cartLine.Quantity += quantityParam;
        }
        /// <summary>
        /// Metoda która usuwa produkt productParam z koszyka
        /// </summary>
        /// <param name="productParam">Produkt</param>
        public void RemoveLine(Product productParam)
        {
            lineCollection.RemoveAll(l => l.Product.ProductId == productParam.ProductId);
        }
        /// <summary>
        /// Metoda liczaca wartość koszyka
        /// </summary>
        /// <returns></returns>
        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(l => l.Product.Price * l.Quantity);
        }
        /// <summary>
        /// Metoda opróżniająca koszyk
        /// </summary>
        public void Clear()
        {
            lineCollection.Clear();
        }
        /// <summary>
        /// Elementy zamowienia
        /// </summary>
        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }

    public class CartLine
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
