﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    /// <summary>
    /// Encja przechwoujaca dane o produkcie
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Id produktu
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// Nazwa produktu
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Opis Produktu
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Cena produktu
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Kategoria produktu
        /// </summary>
        public string Category { get; set; }
    }
}
