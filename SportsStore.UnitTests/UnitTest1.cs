﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SportsStore.WebUI.Models;
using SportsStore.WebUI.HtmlHelpers;
using Microsoft.CSharp;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// test metody List kontrolera Product -> informacje o produktach
        /// </summary>
        [TestMethod]
        public void Can_Paginate()
        {
            //arrange
            Mock<IProductRepository> mockProductRepo = new Mock<IProductRepository>();
            mockProductRepo.Setup(m => m.Products).Returns(new Product[] {
                new Product { ProductId = 1, Name = "P1"},
                new Product { ProductId = 2, Name = "P2"},
                new Product { ProductId = 3, Name = "P3"},
                new Product { ProductId = 4, Name = "P4"},
                new Product { ProductId = 5, Name = "P5"},
            });
            ProductController target = new ProductController(mockProductRepo.Object);
            target.PageSize = 3;
            //act
            ProductsListViewModel result =
                (ProductsListViewModel)target.List(null, 2).Model;
            //assert
            Product[] productsArray = result.Products.ToArray();
            Assert.IsTrue(productsArray.Length == 2);
            Assert.AreEqual(productsArray[0].Name, "P4");
            Assert.AreEqual(productsArray[1].Name, "P5");
        }
        /// <summary>
        /// test metody PageLinks klasy (HtmlHelper) PagingHelpers
        /// </summary>
        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            //arrange
            HtmlHelper myHelper = null;
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };
            Func<int, string> pageUrlDelegate = i => "Page" + i;
            //act
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);
            //assert
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>" +
                @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>" +
                @"<a class=""btn btn-default"" href=""Page3"">3</a>",
                result.ToString());
        }

        /// <summary>
        /// Test metody List kontrolera Product -> dane do stronicowania
        /// </summary>
        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            //arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                    new Product { ProductId = 1, Name = "P1"},
                    new Product { ProductId = 2, Name = "P2"},
                    new Product { ProductId = 3, Name = "P3"},
                    new Product { ProductId = 4, Name = "P4"},
                    new Product { ProductId = 5, Name = "P5"},
            });
            ProductController target = new ProductController(mock.Object);
            target.PageSize = 3;
            int currentPage = 2;
            //act
            ProductsListViewModel result = (ProductsListViewModel)target.List(null, currentPage).Model;
            //assert
            PagingInfo pagingInfo = result.PagingInfo;
            Assert.AreEqual(pagingInfo.CurrentPage, 2);
            Assert.AreEqual(pagingInfo.TotalItems, 5);
            Assert.AreEqual(pagingInfo.ItemsPerPage, 3);
            Assert.AreEqual(pagingInfo.TotalPages, 2);
        }

        /// <summary>
        /// Test metody list kontrolera Product sprawdzający filtrowanie instacnji klasy Products
        /// </summary>
        [TestMethod]
        public void Can_Filter_Products()
        {
            //arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                    new Product { ProductId = 1, Name = "P1", Category = "Cat1"},
                    new Product { ProductId = 2, Name = "P2", Category = "Cat2"},
                    new Product { ProductId = 3, Name = "P3", Category = "Cat1"},
                    new Product { ProductId = 4, Name = "P4", Category = "Cat2"},
                    new Product { ProductId = 5, Name = "P5", Category = "Cat3"},
            });
            ProductController target = new ProductController(mock.Object);
            target.PageSize = 3;
            //act
            Product[] result = ((ProductsListViewModel)target.List("Cat1", 1).Model).
                Products.ToArray();
            //assert
            Assert.AreEqual(2, result.Count());
            Assert.IsTrue(result[0].Name == "P1" && result[0].Category == "Cat1");
            Assert.IsTrue(result[1].Name == "P3" && result[1].Category == "Cat1");
        }

        /// <summary>
        /// Test metody Menu kontrolera Navigation sprawdzajacy tworzenie kategorii
        /// </summary>
        [TestMethod]
        public void Can_Create_Categories()
        {
            //arrange
            Mock<IProductRepository> mockIProductRepository = new Mock<IProductRepository>();
            mockIProductRepository.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductId = 1, Name = "Jabłka Champion", Category = "Jabłka"},
                new Product { ProductId = 2, Name = "Jabłka Gold", Category = "Jabłka"},
                new Product { ProductId = 3, Name = "Śliwki Węgierki", Category = "Śliwki"},
                new Product { ProductId = 4, Name = "Pomarańcze Hiszpania", Category = "Pomarańcze"},
            });
            NavigationController target = new NavigationController(mockIProductRepository.Object);
            //act
            string[] result = ((IEnumerable<string>)target.Menu().Model).ToArray();
            //assert
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("Jabłka",result[0]);
            Assert.AreEqual("Pomarańcze", result[1]);
            Assert.AreEqual("Śliwki", result[2]);
        }

        /// <summary>
        /// Test metody Menu kontrolera Navigation - wysyłanie bieżącej kategorii do widoku przez ViewBag
        /// </summary>
        [TestMethod]
        public void Indicates_Selected_Category()
        {
            //arrange
            Mock<IProductRepository> mockIProductRepository = new Mock<IProductRepository>();
            mockIProductRepository.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductId = 1, Name = "Jabłka Champion", Category = "Jabłka"},
                new Product { ProductId = 2, Name = "Jabłka Gold", Category = "Jabłka"},
                new Product { ProductId = 3, Name = "Śliwki Węgierki", Category = "Śliwki"},
                new Product { ProductId = 4, Name = "Pomarańcze Hiszpania", Category = "Pomarańcze"},
            });
            NavigationController target = new NavigationController(mockIProductRepository.Object);
            string categoryToSelect = "Jabłka";
            //act
            string result = target.Menu(categoryToSelect).ViewBag.SelectedCategory;
            //assert
            Assert.AreEqual(categoryToSelect, result);
        }

        /// <summary>
        /// Test metody List kontrolera Product - zliczanie ilosci produktow wg kategorii
        /// </summary>
        [TestMethod]
        public void Can_Count_Specific_Product_Category()
        {
            //arrange
            Mock<IProductRepository> mockIProductRepository = new Mock<IProductRepository>();
            mockIProductRepository.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductId = 1, Name = "P1", Category = "Cat1"},
                    new Product { ProductId = 2, Name = "P2", Category = "Cat2"},
                    new Product { ProductId = 3, Name = "P3", Category = "Cat1"},
                    new Product { ProductId = 4, Name = "P4", Category = "Cat2"},
                    new Product { ProductId = 5, Name = "P5", Category = "Cat3"}
            });
            ProductController target = new ProductController(mockIProductRepository.Object);
            target.PageSize = 3;
            //act
            int res1 = ((ProductsListViewModel)target.List("Cat1").Model).PagingInfo.TotalItems;
            int res2 = ((ProductsListViewModel)target.List("Cat2").Model).PagingInfo.TotalItems;
            int res3 = ((ProductsListViewModel)target.List("Cat3").Model).PagingInfo.TotalItems;
            int resAll = ((ProductsListViewModel)target.List(null).Model).PagingInfo.TotalItems;
            //assert
            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);
        }
    }
}

