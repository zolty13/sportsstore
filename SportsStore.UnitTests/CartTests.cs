﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.Domain.Entities;
using System.Linq;

namespace SportsStore.UnitTests
{
    /// <summary>
    /// Testowanie encji koszyka na zakupy - Cart
    /// </summary>
    [TestClass]
    public class CartTests
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        /// <summary>
        /// Test metody AddItem klasy Cart - dodawanie produktu
        /// </summary>
        [TestMethod]
        public void Can_Add_New_Lines()
        {
            //arrange
            Product prod1 = new Product { ProductId = 1, Name = "P1" };
            Product prod2 = new Product { ProductId = 2, Name = "P2" };
            Cart target = new Cart();
            //act
            target.AddItem(prod1, 1);
            target.AddItem(prod2, 1);
            CartLine[] result = target.Lines.ToArray();
            //assert
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(prod1, result[0].Product);
            Assert.AreEqual(prod2, result[1].Product);
        }
        /// <summary>
        /// Test metody AddItem klasy Cart - dodawanie produktu ktory jest juz w koszyku
        /// </summary>
        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Product()
        {
            //arrange
            Product prod1 = new Product { ProductId = 1, Name = "P1" };
            Product prod2 = new Product { ProductId = 2, Name = "P2" };
            Cart target = new Cart();
            //act
            target.AddItem(prod1, 1);
            target.AddItem(prod2, 1);
            target.AddItem(prod1, 10);
            CartLine[] result = target.Lines.ToArray();
            //assert
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(11, result[0].Quantity);
            Assert.AreEqual(1, result[1].Quantity);
        }
        /// <summary>
        /// Test metody RemoveLine klasy Cart - usuwanie pozycji zamowienia
        /// </summary>
        [TestMethod]
        public void Can_Delete_Product_From_Cart()
        {
            //arrange
            Product prod1 = new Product { ProductId = 1, Name = "P1" };
            Product prod2 = new Product { ProductId = 2, Name = "P2" };
            Product prod3 = new Product { ProductId = 3, Name = "P3" };
            Cart target = new Cart();
            target.AddItem(prod1, 1);
            target.AddItem(prod2, 3);
            target.AddItem(prod3, 5);
            target.AddItem(prod2, 1);
            //act
            target.RemoveLine(prod2);
            //assert
            Assert.AreEqual(2, target.Lines.Count());
            Assert.AreEqual(0, target.Lines.Where(l => l.Product.ProductId == prod2.ProductId).Count());
        }
        /// <summary>
        /// Test metody ComputeTotalValue klasy Cart - sumowanie wartosci koszyka 
        /// </summary>
        [TestMethod]
        public void Can_Compute_Total_Value()
        {
            //arrange
            Product prod1 = new Product { ProductId = 1, Name = "P1", Price = 100M };
            Product prod2 = new Product { ProductId = 2, Name = "P2", Price = 50M };
            Cart target = new Cart();
            target.AddItem(prod1, 1);
            target.AddItem(prod2, 1);
            target.AddItem(prod2, 3);
            //act
            decimal result = target.ComputeTotalValue();
            //assert
            Assert.AreEqual(300M, result);
        }
        /// <summary>
        /// test metody Clear klasy Cart - czyszczenie koszyka z zawartosci
        /// </summary>
        [TestMethod]
        public void Can_Cler_Cart()
        {
            //arrange
            Product prod1 = new Product { ProductId = 1, Name = "P1", Price = 100M };
            Product prod2 = new Product { ProductId = 2, Name = "P2", Price = 50M };
            Cart target = new Cart();
            target.AddItem(prod1, 1);
            target.AddItem(prod2, 1);
            target.AddItem(prod2, 3);
            //act
            target.Clear();
            //assert
            Assert.AreEqual(0, target.Lines.Count());
        }


    }
}
